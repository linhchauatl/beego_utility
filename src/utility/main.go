package main

import (
	_ "utility/routers"
	"github.com/astaxie/beego"
  "github.com/astaxie/beego/orm"
  _ "github.com/lib/pq"
  "utility/models"
)

func init() {
  orm.RegisterDriver("postgres", orm.DRPostgres)
  orm.RegisterDataBase("default", "postgres", "user=postgres password=development dbname=animals_development sslmode=disable")
  orm.RegisterModel(new(models.Dog))
}

func main() {
	beego.Run()
}
