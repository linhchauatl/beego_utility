package models

type Dog struct {
	Id   int    `json:"id"`
  Age  int    `json:"age"`
	Name string `json:"name"`
	DogAttr string `json:"dog_attr"`
  PackId  int    `json:"pack_id"`
}

func (d Dog) TableName() string {
	return "dogs"
}
