package routers

import (
	"utility/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
    beego.Router("/dogs", &controllers.DogsController{}, "get:Index;post:Create")
}
