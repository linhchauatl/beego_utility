package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
  "utility/models"
)

type DogsController struct {
	beego.Controller
}


func (c *DogsController) Index() {
	db := orm.NewOrm()

	var dogs []models.Dog
	qs := db.QueryTable("dogs")

	limit, _ := c.GetInt("limit")
	offset, _ := c.GetInt("offset")

	qs.Limit(limit, offset).All(&dogs)
	c.Data["json"] = &dogs
	c.ServeJSON()
}

func (c *DogsController) Create() {
	db := orm.NewOrm()

	var dogs []models.Dog
	qs := db.QueryTable("dogs")

	limit, _ := c.GetInt("limit")
	offset, _ := c.GetInt("offset")

	qs.Limit(limit, offset).All(&dogs)
	c.Data["json"] = &dogs
	c.ServeJSON()
}
