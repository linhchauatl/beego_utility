--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4
CREATE DATABASE animals_development
    WITH
    TEMPLATE = template0
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

\connect animals_development;
SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cats (
    id integer NOT NULL,
    age integer,
    name character varying(40) NOT NULL,
    animal_type character varying(40),
    cat_attr character varying(60),
    stripes integer
);


ALTER TABLE cats OWNER TO postgres;

--
-- Name: cats_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cats_id_seq OWNER TO postgres;

--
-- Name: cats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cats_id_seq OWNED BY cats.id;


--
-- Name: claws; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE claws (
    id integer NOT NULL,
    cat_id integer NOT NULL,
    claw_value integer NOT NULL
);


ALTER TABLE claws OWNER TO postgres;

--
-- Name: claws_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE claws_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE claws_id_seq OWNER TO postgres;

--
-- Name: claws_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE claws_id_seq OWNED BY claws.id;


--
-- Name: dogs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE dogs (
    id integer NOT NULL,
    age integer,
    name character varying(40) NOT NULL,
    dog_attr character varying(60),
    pack_id integer
);


ALTER TABLE dogs OWNER TO postgres;

--
-- Name: dogs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dogs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dogs_id_seq OWNER TO postgres;

--
-- Name: dogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dogs_id_seq OWNED BY dogs.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE schema_migrations OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer NOT NULL,
    login character varying(255),
    email character varying(255),
    crypted_password character varying(40),
    salt character varying(40),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    remember_token character varying(255),
    remember_token_expires_at timestamp without time zone
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cats ALTER COLUMN id SET DEFAULT nextval('cats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY claws ALTER COLUMN id SET DEFAULT nextval('claws_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dogs ALTER COLUMN id SET DEFAULT nextval('dogs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: cats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cats (id, age, name, animal_type, cat_attr, stripes) FROM stdin;
1	2	Cat 1	\N	Cat 1 attr	\N
2	4	Cat 2	\N	Cat 2 attr	\N
3	3	TabbyCat 1	TabbyCat	TabbyCat 1 attr	100
4	5	TabbyCat 2	TabbyCat	TabbyCat 2 attr	120
\.


--
-- Name: cats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cats_id_seq', 4, true);


--
-- Data for Name: claws; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY claws (id, cat_id, claw_value) FROM stdin;
1	1	0
2	1	10
3	1	18
4	1	2
5	2	17
6	2	17
7	2	12
8	2	1
9	3	10
10	3	18
11	3	3
12	3	17
13	4	13
14	4	9
15	4	15
16	4	2
\.


--
-- Name: claws_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('claws_id_seq', 16, true);


--
-- Data for Name: dogs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY dogs (id, age, name, dog_attr, pack_id) FROM stdin;
1	2	Dog 1	Belongs to pack 1	1
2	3	Dog 2	Belongs to pack 1	1
3	4	Dog 3	Belongs to pack 1	1
4	2	Dog 4	Belongs to pack 2	2
5	3	Dog 5	Belongs to pack 2	2
6	4	Dog 6	Belongs to pack 2	2
7	3	Dog 7	Belongs to no pack	\N
\.


--
-- Name: dogs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dogs_id_seq', 7, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20080926213814
20080926213816
20080926213818
20080926225304
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, login, email, crypted_password, salt, created_at, updated_at, remember_token, remember_token_expires_at) FROM stdin;
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 1, false);


--
-- Name: cats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cats
    ADD CONSTRAINT cats_pkey PRIMARY KEY (id);


--
-- Name: claws_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY claws
    ADD CONSTRAINT claws_pkey PRIMARY KEY (id);


--
-- Name: dogs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dogs
    ADD CONSTRAINT dogs_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_claws_on_cat_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_claws_on_cat_id ON claws USING btree (cat_id);


--
-- Name: index_dogs_on_pack_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_dogs_on_pack_id ON dogs USING btree (pack_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
