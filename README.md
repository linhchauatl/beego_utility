## Beego utility
A Go web-based application using Beego.

The application uses
- Go programming language
- Beego webframework
- PostgreSQL database

### I. Preparation to run on local machine

**Note**: If you don't want to install too much stuffs on your local machine, please skip to the section **III. Run development environment from a docker container**.

- Install Go programming language
- Install PostgreSQL
- Use git to clone this repo
- Install beego
```
go get github.com/beego/bee
```
- Create a database named `animals_development` similar to the database of [this Rails application](https://github.com/linhchauatl/animals).
You can actually clone the Rails application `animals`, and run `rake db:create db:migrate` to create the database and seed data.

### II. Run application from local machine

**1.Run from source**

  Setup environment
  ```
  # PATH_TO_BEEGO_UTILITY is where you clone the application
  cd <PATH_TO_BEEGO_UTILITY>
  source setup_env.sh
  ```

  Then you can go into the directory `src/utility` and run it:

  ```
  cd src/utility
  go run main.go
  ```

  The URL is: `http://localhost:4000`.<br/>
  You can try: `http://localhost:4000/dogs`

**2.Build executable file**

  Setup environment
  ```
  cd <PATH_TO_BEEGO_UTILITY>
  source setup_env.sh
  ```

  Then you can go into the directory `src/utility` and build it:

  ```
  cd src/utility
  go build main.go
  ```

  It will create a binary executable called 'main'.
  You can run `main` from anywhere as if you call any other executable.

### III. Run development environment from a docker container

  **1.Prerequisites**

  You must use git to clone the application to your local machine.

  You must have [docker](https://docs.docker.com/engine/installation/) installed on your machine.<br/>
  For Mac users:<br/>
    - Install [Docker Toolbox](https://www.docker.com/products/docker-toolbox).<br/>
    - Add this line to your $HOME/.bash_profile: `eval $(docker-machine env default)`<br/>
    - Create default docker-machine: Open terminal, run this command `docker-machine create --driver virtualbox default`<br/>
    - It will take awhile to build the default machine the first time, so please feel free to get a coffee.<br/>

  **2.Build the container**

  This is run only for the first time you build the container, or when you change the Dockerfile.
  Open the terminal on your local machine and run the command:
  ```
  # PATH_TO_BEEGO_UTILITY is where you clone the application
  cd <PATH_TO_BEEGO_UTILITY>
  docker build -t utility:latest .
  ```

  **3.Run the container and run the server**

  Open the terminal to run commands.

  - Launch the container:
  ```
  bin/run_container.sh
  ```

  - Connect to the container:
  ```
  bin/ssh_to_container.sh
  ```

  Once you are in the container, you are in the directory `kratos`. It is where your Go workspace on your host machine is mounted in the container.

  **You must run this command inside `/kratos` the first time you ssh to the container**:
  ```
  go get github.com/beego/bee
  ```

  - Run the Go server:
  ```
  cd /kratos/src/utility
  go run main.go
  ```

  **4.Connect to the Go application running in container**

  - If your local machine is running Linux, you can directly connect to the Go app:
  ```
  http://localhost:4000
  http://localhost:4000/dogs
  ```

  - If you use Mac, on local Mac, open another terminal, run the command `docker-machine ls` to see the IP address of your docker machine. You will see something similar to:
  ```
  NAME      ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER    ERRORS
  default   *        virtualbox   Running   tcp://192.168.99.100:2376           v1.10.1
  ```

  Please look at the IP `192.168.99.100` in URL. Your application will be accessible via:
  ```
  http://192.168.99.100:4000
  http://192.168.99.100:4000/dogs
  ```

  **5.Write code and test**

  The codebase directory on your local machine is shared into the container. So you can write code on your local machine, and restart the web server on the container to test the new code.
