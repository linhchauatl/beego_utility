# Build from CentOS 7, install Go, Martini and PostgreSQL.
FROM chauhonglinh/centos_postgres_go:latest
RUN echo "export PATH=\$PATH:.:$HOME/bin:/usr/local/go/bin" >> $HOME/.bash_profile
RUN su - postgres -c "/usr/pgsql-9.6/bin/pg_ctl initdb -D /var/lib/pgsql/9.6/data"
COPY ./animals_development.sql /tmp
RUN /bin/bash -l -c "su - postgres -c '/usr/pgsql-9.6/bin/pg_ctl -D /var/lib/pgsql/9.6/data -l logfile start' &&\
                     su - postgres -c '/usr/bin/psql < /tmp/animals_development.sql'"
RUN echo "cd /kratos && source setup_env.sh" >> $HOME/.bash_profile
ENTRYPOINT /bin/bash -l -c "su - postgres -c '/usr/pgsql-9.6/bin/pg_ctl -D /var/lib/pgsql/9.6/data -l logfile start' && /bin/bash"
